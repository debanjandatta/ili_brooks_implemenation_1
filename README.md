# README #
http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004382

Data can be downloaded from : https://gis.cdc.gov/GRASP/Fluview/FluHospRates.html

------------------------------------------------------
Preferred:

API endpoint
https://github.com/cmu-delphi/delphi-epidata#influenza-data

example:
http://delphi.midas.cs.cmu.edu/epidata/api.php?source=fluview&regions=nat&epiweeks=201530

Params : <br/>  
source=fluview	<br/>  
regions=nat 	<br/>  
List of regions : nat
hhs1
hhs2
hhs3
hhs4
hhs5
hhs6
hhs7
hhs8
hhs9
hhs10
cen1
cen2
cen3
cen4
cen5
cen6
cen7
cen8
cen9


epiweeks=201530	<br/> Format :  YYYYWW | [201440,201501-201510] 

-----

Curve Estimation done using :
http://contrib.scikit-learn.org/py-earth/content.html#multivariate-adaptive-regression-splines
