import requests
import utils
import schema
import pandas as pd
import random
import os


# Usage :
#   region :  national or one of the hhs regions (See Readme )
#   year :  Single  year
#   weeks : list of weeks, or a range or a single week
#           | int week | range string w1-wn | list[w1,w2..wn]
def get_api(region, years, weeks):

    host = "http://delphi.midas.cs.cmu.edu/epidata/api.php?"
    src = "fluview"
    params = {}
    params["fmt"] = "json"
    params["source"] = src
    params["regions"] = region
    multi_year = False

    # Convert weeks into a list forcefully(if it isn't), if its a single year for ease of code
    if "-" in weeks:
        wks = weeks.split("-")

        if int(wks[0]) < int(wks[1]):
            # week range inside a single year
            weeks = range(int(wks[0]), int(wks[1]) + 1)

        else:
            # week range across a season of 2 years
            multi_year = True
            weeks_yr0 = range(int(wks[0]), utils.MAX_WEEKS)
            weeks_yr1 = range(1, int(wks[1]) + utils.MIN_WEEKS)


    elif isinstance(weeks, int):
        weeks = [weeks]

    # Convert years into a list forcefully(if it isn't), if its a single year for ease of code
    if isinstance(years, str) and "-" in years:
        years = years.split("-")
    elif not isinstance(years, list) and multi_year:
        # A single year has been passed in case of multi-year - viz. the start year of the season
        tmp_y = years
        years = []
        years.append(tmp_y)
        years.append(years[0] + 1)

    #List of epiweeks to be passed in parameter
    ew_list = []
    if multi_year:
        for idx, item in enumerate(weeks_yr0):
            weeks_yr0[idx] = str(item).zfill(2)
        for idx, item in enumerate(weeks_yr1):
            weeks_yr1[idx] = str(item).zfill(2)
        ew_list.extend([str(years[0]) + str(week) for week in weeks_yr0]);
        ew_list.extend([str(years[1]) + str(week) for week in weeks_yr1]);
    else:
        # Ensure that week numbers are in format 01,02....52
        for idx, item in enumerate(weeks):
            weeks[idx] = str(item).zfill(2)
        for year in years:
            ew_list.extend([str(year) + str(week) for week in weeks]);

    params["epiweeks"] = ','.join( str(week) for week in ew_list )
    return host, params

# Make the API call
def get_data(region="nat", years=[], weeks="1-52"):
    host, params = get_api(region, years, weeks);
    try:
        req = requests.get(host, params=params)
        if (req.status_code != 200):
            raise Exception('Non 200 http status code!!')
    except:
        print 'Error in API call'
        print 'Status code : ' + str(req.status_code)
    return req.json()

# Parse the json data
def process_data(json_data, output_file):

    columns = list(schema.raw_data_columns)
    age_grp_nums = range(6);
    age_grps = ["age_" + str(a) for a in age_grp_nums]
    columns.extend(age_grps)
    master_df = pd.DataFrame(columns=columns)
    data = json_data['epidata']

    for frame in data:
        dict = {}
        dict['wili'] = frame['wili']
        year_ew = str(frame['epiweek'])
        dict['ew'] = int(year_ew[4:6])
        dict['year'] = int(year_ew[0:4])
        dict['num_ili'] = frame['num_ili']
        dict['num_patients'] = frame['num_patients']
        for x in age_grp_nums:
            key1 = 'age_' + str(x)
            key2 = 'num_age_' + str(x)
            dict[key1] = frame[key2]

        df = pd.DataFrame(dict, columns=columns, index=[0])
        master_df = master_df.append(df, ignore_index=True)

    master_df.to_csv(output_file, mode='w',index=False)

    #return if there is 52 weeks of data (True) or not (False)
    if len(master_df.index) < utils.MAX_WEEKS - 1:
        return False;
    return True;

# Before 2003, some weeks data was not available
# Fill up those weeks data points using the data from next years
# Choose at random
# Also save files with selected columns only , in Filled
# Add Season column
def backfill_data(needs_backfill,years,region):
    last_yr = max(needs_backfill)
    #years with complete data
    basis_yrs = range(last_yr+1,utils.max_year+1)
    data_dict = {}

    for y in basis_yrs:
        df = pd.read_csv(utils.get_file_name(y,region,'Raw'))
        data_dict[y] = df;
        #Place the file in Filled
        src_file = utils.get_file_name(y,region,'Raw')
        dest_file = utils.get_file_name(y,region,'Filled')
        os.system('cp ' + src_file + ' ' + dest_file)

    for y in needs_backfill:
        df_y = pd.read_csv(utils.get_file_name(y,region,'Raw'),index_col=None)

        #the first ew for the season
        first_ew = df_y.iloc[0].ew;
        #the last ew for the season
        last_ew = df_y.iloc[len(df_y)-1].ew;

        # ew records cycle each year. Hence...
        list_ew = range(int(last_ew+1),int(first_ew))
        df = pd.DataFrame(df_y.columns)

        for ew in list_ew :
            # select a  value from the others
            y_idx = random.choice(basis_yrs)
            base_df = data_dict[y_idx]
            row = base_df.loc[base_df['ew']==ew]
            row.year = y
            df_y = df_y.append(row,ignore_index=True)

        df_y = df_y.sort_values(by=['year','ew'],ascending=[1,1])
        #write out the filled dat frame
        dir = 'Filled'
        df_y.to_csv(utils.get_file_name(y, region, dir),index=False)

# Epi week starts from week 21
# Shift the ew to start from 1
# name of attribute : ew
# Input :
# year (int or list)
# region (str)
def set_ew_shifted(year,region):

    def rectify_ew(x):
        if x.ew  > 20 :
            return x.ew - 20
        return x.ew + 32

    dir = 'Filled'
    years = []
    years.extend(year)

    for y in years:
        file = utils.get_file_name(y,region,dir)
        df = pd.read_csv(file)
        df['ew'] = df.apply(rectify_ew, axis=1);
        df.to_csv(file,index=False)
    return;

def execute():
    years = range( utils.min_year , utils.max_year +1 )
    years.reverse()
    years_strs = [ utils.get_years_string(y) for y in years ]
    needs_backfill = []

    #find the years that need backfill
    for year,yr_str in zip(years,years_strs):
        weeks = "21-20"
        region = "nat"
        file = utils.get_file_name(year,region,'Raw')
        data = get_data(region, year, weeks)
        if not process_data(data, file):
             needs_backfill.append(year)
    backfill_data(needs_backfill,years,region);
    set_ew_shifted(years,region)


execute()

