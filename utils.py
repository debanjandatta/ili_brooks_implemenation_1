import os

#Constants
MIN_WEEKS = 1
MAX_WEEKS = 53
min_year = 2000
max_year = 2015
start_ew = 21
max_ew_curr_yr = 32
errors_file = 'Data/Priors/prior_errors.csv'
peaks_file = 'Data/Priors/prior_peaks.csv'
current_year = 2015

#input : integer y1
#output : string of the form 'y1-y1+1'
def get_years_string(year):
    return  str(year) + "-" +str(year+1);

def get_season(year):
    return str(year)+ '-' + str(year+1);

def get_file_name(year,region,dir = None):
    year_str = get_years_string(int(year))

    if dir is None:
        return year_str + '_' + region + '.csv'
    dir = 'Data/'+dir

    if not os.path.exists(dir):
        os.makedirs(dir);
    return dir + '/' + year_str + '_' + region + '.csv'