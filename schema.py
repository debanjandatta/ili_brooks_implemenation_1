raw_data_columns = ['year', 'ew', 'wili', 'num_ili','num_patients']
prior_struc = ['year','ew','wili','error']
prior_error_columns = [ 'season','error']
peaks_schema = ['year','peak_value','peak_week']
curve_columns = ['year','ew','wili']
gen_curve_columns = ['year','ew','wili']