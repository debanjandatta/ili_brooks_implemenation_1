import schema
import utils
import pandas as pd
import random
from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import math

EXCLUDE_09_H1N1 = True

err_list = None
peak_wili_min = None
peak_wili_max = None
peak_ew_min = None
peak_ew_max = None
sd_list = None
max_iterations = 100


def init():
    global sd_list, peak_wili_min, peak_wili_max, peak_ew_min, peak_ew_max
    # select the noise (standard deviation) from the list f previous ones at random
    file = utils.errors_file;
    df_noise = pd.read_csv(file)
    sd_list = df_noise['error']

    # select the peak value uniformly from a range
    # select the peak week uniformly from a range
    file = 'Data/Priors/prior_peaks.csv'
    df_peaks = pd.read_csv(file)
    peak_ew_min = df_peaks['peak_week'].min();
    peak_ew_max = df_peaks['peak_week'].max();

    peak_wili_min = df_peaks['peak_value'].min();
    peak_wili_max = df_peaks['peak_value'].max();


def get_peak_ew():
    return random.randint(peak_ew_min, peak_ew_max);


def get_peak_wili():
    return random.uniform(peak_wili_min, peak_wili_max);


def get_pace():
    # select the pace/scale
    return random.uniform(0.75, 1.25);


def get_sd():
    return 10 * random.choice(sd_list)


# Function to return :
# a curve
# a standard dev(chosen uniformly from past)
def get_single_curve(current_year, region,baseline):
    # select a curve from the past years
    hist_years = range(utils.min_year, current_year)
    y = random.choice(hist_years)
    dir = 'Priors'
    file = utils.get_file_name(y, region, dir)
    df_prior = pd.read_csv(file)

    peak_wili = get_peak_wili()
    peak_ew = get_peak_ew()
    pace = get_pace()
    sd = get_sd()

    prior_peak_wili = df_prior.wili.max();
    index = ((df_prior.loc[df_prior['wili'] == prior_peak_wili]).index.tolist())[0]
    prior_peak_week = df_prior.loc[index, 'ew']

    ew_list = range(1, utils.MAX_WEEKS)
    df = pd.DataFrame(columns=schema.gen_curve_columns)
    for i in ew_list:
        # From prior get the peak week
        w = i - peak_ew  # if (i-peak_ew) > 0 else i
        w = w / pace
        w = round(w + prior_peak_week)
        # Sanity check!
        if (w < 1 or w > 52):
            w = i

        idx = (df_prior.loc[df_prior['ew'] == w].index.tolist())[0]
        wili_i = df_prior.loc[idx, 'wili']

        # print (peak_wili - baseline)/(prior_peak_wili - baseline)
        wili_i = wili_i - baseline
        scale = (peak_wili - baseline) / (prior_peak_wili - baseline)
        wili_i = (scale) * wili_i + baseline
        df = df.append({'year': current_year, 'ew': i, 'wili': wili_i}, ignore_index=True)

    df = df.reset_index()
    return df, sd


# Input :
# curr_year is start of the current season year
# curr_week

def get_curves(curr_year, region, curr_ew_week, baseline):
    global max_iterations
    #  Get the current(target) year data
    dir = 'Filled'
    file = utils.get_file_name(curr_year, region, dir)
    curr_df = pd.read_csv(file, usecols=schema.curve_columns)
    # slice the dataframe till current week
    curr_df = curr_df.loc[(curr_df.ew <= curr_ew_week)]
    list_curve = []
    list_wt = []

    for i in range(max_iterations):
        wt = 0
        prior, sd = get_single_curve(curr_year, region,baseline)
        for i in range(1, curr_ew_week + 1):
            idx = (prior.loc[prior['ew'] == i].index.tolist())[0]
            mean = prior.loc[idx, 'wili']
            # current point
            norm_dist = norm(mean, sd)
            pdf = norm_dist.pdf(curr_df.loc[i - 1].wili)

            # print 'Mean ' + str(mean) + ' ' + ' sd: ' + str(sd) + ' || ' + str(
            #     curr_df.loc[i - 1].wili) + ' || ' + ' weight ' + str(pdf)

            # Paper asks to multiply weights
            # Implementation here based on log
            if (pdf > 0):
                wt += math.log(pdf)

        # for weeks previous to & upto curr_week
        # replace the prior values with current values
        # that forms a candidate curve
        candidate = pd.DataFrame(curr_df, index=None)
        tmp = prior[(prior.ew > curr_ew_week)]
        candidate = candidate.append(tmp, ignore_index=True)
        list_curve.append(candidate);
        list_wt.append(wt);
    return list_curve, list_wt


def calculate_posterior_mean(curr_year, region, curr_ew_week, baseline):
    list_df, list_wt = get_curves(curr_year, region, curr_ew_week, baseline)

    # normalize weights
    total = sum(list_wt)
    list_wt = [(wt / total) for wt in list_wt]
    list_ew = range(1, utils.MAX_WEEKS)
    # for each ew calculate point
    y = []
    for i in list_ew:
        s = 0
        curve_idx = 0
        for df in list_df:
            wt = list_wt[curve_idx]
            s = s + df.loc[i - 1, 'wili'] * 1;
            curve_idx += 1
        mean = s / (curve_idx + 1)
        y.append(mean)
    return y


def plot(x, y_pred, y_actual,curr_year,curr_ew_week):
    plt.figure()
    plt.plot(x, y_pred, 'r-')
    plt.plot(x, y_actual, 'b.')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title(' Curve fitting ')
    red = mpatches.Patch(color='red', label='Predicted data points')
    blue = mpatches.Patch(color='blue', label='Real data points')
    plt.legend(handles=[red, blue])
    file = 'Plots/result_'+utils.get_years_string(curr_year) +'_'+ str(curr_ew_week)+'.png'
    plt.savefig(file)
    #plt.show()
    return


def test(curr_ew_week, baseline):
    region = "nat"
    curr_year = 2015
    y = calculate_posterior_mean(curr_year, region, curr_ew_week, baseline)
    x = range(1, utils.MAX_WEEKS)
    file = utils.get_file_name(curr_year, region, 'Filled')
    df = pd.read_csv(file, usecols=['ew', 'wili'])
    y_actual = df['wili'].tolist()
    plot(x, y, y_actual,curr_year,curr_ew_week);
    # calculate error in next 4 weeks
    err = 0
    for i in range(4):
        idx = curr_ew_week + i
        if (idx > 51):
           continue;
        err += math.pow( y[idx] - y_actual[idx],2 )
    return math.sqrt(err)


def plot_errors(x,y):
    plt.figure()
    plt.plot(x, y, 'r-')
    plt.plot(x, y, 'b.')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title(' Errors ')
    plt.show()
    return

def main():
    baseline = 0.20
    init();

    err_list = []
    for curr_ew_week in range(1, utils.MAX_WEEKS):
        err = test(curr_ew_week, baseline);
        err_list.append(err)

    plot_errors(range(1,utils.MAX_WEEKS),err_list);


main()


