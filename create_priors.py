from pyearth import Earth
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import schema
import utils
import matplotlib.patches as mpatches
import math



def fit_curve(x,y):
    output_weight = np.ones([52])
    for i in range(10, 40):
        output_weight[i] = 1 * output_weight[i];
    # Print to debug
    # print(model.trace())
    # print(model.summary())
    model = Earth(max_degree=2)
    model.fit(x, y, output_weight)
    return  model.predict(x)


def get_curve(year, region, show_plot=False):
    columns = schema.curve_columns
    dir = 'Filled'
    file = utils.get_file_name(year, region, dir)
    df = pd.read_csv(file, usecols=columns)
    df['error'] = 0

    #   Season starts at week 21 and ends at following year's week 20
    #   21 - 39 is preseason
    #   40 - 20 is the actual season

    x = df.index.values
    y = df['wili']

    output_weight = np.ones([52])
    for i in range(10, 40):
        output_weight[i] = 1 * output_weight[i];

    y_new = fit_curve(x,y)
    diff_y = y_new - y
    diff_y = [k * k for k in diff_y]
    df['error'] = diff_y

    avg_error =  math.sqrt(sum(diff_y) / len(diff_y))
    df['wili'] = y_new

    if show_plot:
        plt.figure()
        plt.plot(x, y, 'r-')
        plt.plot(x, y_new, 'b.')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title(' Curve fitting ')
        red = mpatches.Patch(color='red', label='Actual data points')
        blue = mpatches.Patch(color='blue', label='Predicted data points')
        plt.legend(handles=[red, blue])
        plt.title(' Year '+ utils.get_years_string(year))
        plt.show()
    return df, avg_error


# Returns :
# List of Prior curves, DataFrame of prior errors by season
def collect_curves(years, region):
    def rectify_year(row):
        if row.ew > utils.max_ew_curr_yr:
            return row.year + 1
        else:
            return row.year

    prior_errors = pd.DataFrame(columns=schema.prior_error_columns)
    if region is None:
        region = "nat"
    # Dict of season :: dataframe
    prior_curves = {}
    for y in years:
        df, prior_error = get_curve(y, region)
        df['year'] = y;
        df['year'] = df.apply(rectify_year, axis=1);
        season = utils.get_season(y)
        prior_curves[season] = df
        prior_errors = prior_errors.append({'season': season, 'error': prior_error}, ignore_index=True);
    return prior_curves, prior_errors


# This method gets the curves, calculates peak weak and peak values.
# Returns :
# curves [dataframe] , errors [dataframe] , peaks [dataframe]
def process_curves():
    years = range(utils.min_year, utils.max_year)
    region = "nat"
    prior_curves, prior_errors = collect_curves(years, region)
    peaks = pd.DataFrame(columns=schema.peaks_schema)
    # Create a list of peaks
    years_str = [ utils.get_years_string(y) for y in years ]
    for y in years:
        season = utils.get_season(y)
        # df_temp = prior_curves.xs(y)
        df_temp = prior_curves[season]
        df_temp = df_temp.reset_index()
        peak_value = df_temp.wili.max();
        index = ((df_temp.loc[df_temp['wili'] == peak_value]).index.tolist())[0]
        peak_week = df_temp.loc[index, 'ew']
        dict = {'year': y,
                'peak_value': peak_value,
                'peak_week': peak_week}
        peaks = peaks.append(dict, ignore_index=True)
    dir  = 'Priors'
    for season in prior_curves.keys():
        file = utils.get_file_name(prior_curves[season].year.min(),region,dir)
        prior_curves[season].to_csv(file,mode='w',index=False)
    prior_errors.to_csv(utils.errors_file,mode='w',index=False)
    peaks.to_csv(utils.peaks_file, mode='w', index=False)
    return;


process_curves()
#get_curve(2002, "nat", show_plot=True)